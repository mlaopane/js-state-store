export interface Action {
    type: string;
    payload: any;
}

export type Reducer<T> = (state: T, action: Action) => T;

export class Store {
    private history = [];
    private offset = -1;

    constructor(private reducer, private state) {
        this.history.push(state);
        this.offset++;
    }

    dispatch(action: Action) {
        this.state = this.reducer(this.state, action);
        this.history.splice(this.offset + 1);
        this.history.push(this.state);
        this.offset++;
        return this;
    }

    getState() {
        return this.state;
    }

    getHistory() {
        return this.history;
    }

    revert() {
        if (this.offset > 0) {
            this.state = this.history[this.offset - 1];
            this.history.splice(this.offset, 1);
            this.offset--;
        }
        return this;
    }

    back() {
        if (this.offset > 0) {
            this.state = this.history[this.offset - 1];
            this.offset--;
        }
        return this;
    }

    next() {
        const nextState = this.history[this.offset + 1];
        if (undefined !== nextState) {
            this.state = nextState;
            this.offset++;
        }
        return this;
    }
}

/**
 * @param reducer
 * @param state
 */
export function createStore<T>(reducer: Reducer<T>, state: any = {}): Store {
    const store = new Store(reducer, state);
    return store;
}