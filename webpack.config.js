const path = require("path")
const Html = require('html-webpack-plugin');
const Clean = require('clean-webpack-plugin');
const TsConfigPaths = require('tsconfig-paths-webpack-plugin');

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000
    },
    plugins: [
        new Html({
            template: './dist/index.html',
            inject: 'body',
        }),
        new Clean(),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            },
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        plugins: [
            new TsConfigPaths()
        ],
        extensions: [".tsx", ".ts", ".js", ".jsx", ".json"]
    }
}